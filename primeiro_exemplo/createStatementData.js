export default function createStatementData(invoice, plays) {
    const result = {};
    result.customer = invoice.customer;
    result.performances = invoice.performances.map(enrichPerformance);
    result.totalVolumeCredits = totalVolumeCreditsFor(result);
    result.totalAmount = totalAmountFor(result);
    return result;

    function enrichPerformance(performance) {
        const calculator = new performanceCalculatorFactory(performance, playFor(performance));
        const result = Object.assign({}, performance);
        result.play = calculator.play;
        result.amount = calculator.amount;
        result.volumeCredits = calculator.volumeCredits;
        return result;
    }

    function playFor(performance) {
        return plays[performance.playID]
    }

    function totalAmountFor(data) {
        return data.performances.reduce((total, p) => total + p.amount, 0);
    }

    function totalVolumeCreditsFor(data) {
        return data.performances.reduce((total,p) => total + p.volumeCredits, 0);
    }
}

function performanceCalculatorFactory(performance, play) {
    switch (play.type) {
        case "tragedy": return new TragedyCalculator(performance, play);
        case "comedy": return new ComedyCalculator(performance, play);
        default:
            throw new Error(`unknow type: ${play.type}`)
    }
}

class PerformanceCalculator {
    constructor(performance, play) {
        this.performance = performance;
        this.play = play;
    }

    get amount() {
        // Método abstrato em JS
        throw new Error('Responsabilidade da subclasse!!!');
    }
    
    get volumeCredits() {
        let result = 0;
        result += Math.max(this.performance.audience - 30, 0);
        return result;
    }

}

class TragedyCalculator extends PerformanceCalculator {
    get amount(){
        let result = 40000; 
            if (this.performance.audience > 30) {
                result += 1000 * (this.performance.audience - 30);
            }
        return result;
    }
}

class ComedyCalculator extends PerformanceCalculator {
    get amount() {
        let result = 30000;
        if (this.performance.audience > 20) {
            result += 10000 + 500 * (this.performance.audience - 20);
        }
        result += 300 * this.performance.audience;
        return result;
    }

    get volumeCredits() {
        let result = super.volumeCredits;
        result += Math.max(this.performance.audience - 30, 0);
        return result;
    }
}
