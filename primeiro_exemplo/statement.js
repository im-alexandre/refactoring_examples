import fs from 'fs';
import createStatementData from './createStatementData.js';

function htmlStatement(invoice,plays){
    return renderHTML(createStatementData(invoice,plays));
}

function renderHTML(data) {
    let result = `<h1>Statement for ${data.customer}</h1>\n`;
    result += "<table>\n";
    result += "<thead>\n";
    result += "<tr><th>play</th><th>seats</th><th>cost</th></tr>\n";
    result += "</thead>\n";
    result += "<tbody>\n";
    for (let perf of data.performances) {
        result += `<tr><td>${perf.play.name}</td><td>${perf.audience}</td>`;
        result += `<td>${usdCurrency(perf.amount)}</td></tr>\n`;
    }
    result += "</tbody>\n";
    result += "</table>\n";
    result += `<p>Amount owed is <em>${usdCurrency(data.totalAmount)}</em><p>\n`;
    result += `<p>You earned <em>${data.totalVolumeCredits}</em> credits </p>\n`;
    return result;
}

function statement(invoice, plays) {
    return renderPlainText(createStatementData(invoice, plays));
}

function renderPlainText(data) {
    let result = `Statement for ${data.customer}\n`;

    for (let perf of data.performances) {
        result += ` ${perf.play.name}: ` +
            `${usdCurrency(perf.amount)} (${perf.audience} seats)\n`;
    }
    result += `Amount owed is ${usdCurrency(data.totalAmount)}\n`;
    result += `You earned ${data.totalVolumeCredits} credit\n`;
    return result;
}

function usdCurrency(Number) {
    return new Intl.NumberFormat("en-US", 
        {style: "currency", currency: "USD",
            minimumFractionDigits: 2}).format(Number/100);
}

var plays = JSON.parse(fs.readFileSync('./plays.json', 'utf-8'));
var invoice = JSON.parse(fs.readFileSync('./invoices.json', 'utf-8'));

console.log(htmlStatement(invoice=invoice, plays=plays));
